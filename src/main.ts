export async function delay(milliseconds: number) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, milliseconds);
    });
}

export enum Estado {
    dormindo, esperando, comendo
}

export class Filosofo {
    private _estado: Estado;
    private _garfoA: Garfo | null;
    private _garfoB: Garfo | null;

    public get estado(): Estado {
        return this._estado;
    }

    public get garfoA(): Garfo | null {
        return this._garfoA;
    }

    public get garfoB(): Garfo | null {
        return this._garfoB;
    }

    public constructor() {
        this._estado = Estado.esperando;
        this._garfoA = null;
        this._garfoB = null;
    }

    public isDormindo() {
        return this._estado == Estado.dormindo;
    }

    public isComendo() {
        return this._estado == Estado.comendo;
    }

    public isMaosOcupadas() {
        return this.garfoA != null && this.garfoB != null;
    }

    public async dormir() {
        if (this.isDormindo()) {
            throw new Error();
        }
        this._estado = Estado.dormindo;
        await delay(this.gerarIntervalo());
        this._estado = Estado.esperando;
    }

    public async comer() {
        if (this.isComendo()) {
            throw new Error();
        }
        if (!this.isMaosOcupadas()) {
            throw new Error();
        }
        this._estado = Estado.comendo;
        await delay(this.gerarIntervalo());
        this._estado = Estado.esperando;
        this.soltarGarfos();
    }

    public async pegar(garfo: Garfo) {
        if (this.isMaosOcupadas()) {
            throw new Error();
        }
        if (garfo.isOcupado()) {
            await delay(100);
            await this.pegar(garfo);
            return;
        }
        garfo.pegar(this);
        if (this.garfoA == null) {
            this._garfoA = garfo;
        }
        else {
            this._garfoB = garfo;
        }
    }

    private soltarGarfos() {
        if (this.garfoA != null) {
            this.garfoA.soltar(this);
            this._garfoA = null;
        }
        if (this.garfoB != null) {
            this.garfoB.soltar(this);
            this._garfoB = null;
        }
    }

    private gerarIntervalo(): number {
        return Math.random() * 1000;
    }
}

export class Garfo {
    private _filosofo: Filosofo | null;

    public get filosofo(): Filosofo | null {
        return this._filosofo;
    }

    public constructor() {
        this._filosofo = null;
    }

    public isOcupado(): boolean {
        return this.filosofo != null;
    }

    public pegar(filosofo: Filosofo) {
        if (this.filosofo != null) {
            throw new Error();
        }
        this._filosofo = filosofo;
    }

    public soltar(filosofo: Filosofo) {
        if (this.filosofo != filosofo) {
            throw new Error();
        }
        this._filosofo = null;
    }
}

export class Jantar {
    private _filosofos: Filosofo[];
    private _garfos: Garfo[];
    private _comecou: boolean;
    public delegate: JantarDelegate | null;

    public get filosofos(): Filosofo[] {
        return this._filosofos;
    }

    public get garfos(): Garfo[] {
        return this._garfos;
    }

    public get comecou(): boolean {
        return this._comecou;
    }

    public constructor(quantidade: number) {
        this._filosofos = [];
        this._garfos = [];
        this._comecou = false;
        this.delegate = null;
        for (var i = 0; i < quantidade; i++) {
            this._filosofos.push(new Filosofo());
            this._garfos.push(new Garfo());
        }
    }

    public comecar() {
        if (this.comecou) {
            throw new Error();
        }
        this.filosofos.forEach((filosofo) => {
            this.rodar(filosofo);
        });
    }

    private async rodar(filosofo: Filosofo) {
        let indice = this.filosofos.indexOf(filosofo);
        if (this.delegate != null) {
            this.delegate.dormiu(indice);
        }
        await filosofo.dormir()
        if (this.delegate != null) {
            this.delegate.acordou(indice);
        }
        await filosofo.pegar(this.garfoA(filosofo));
        if (this.delegate != null) {
            this.delegate.pegouGarfoA(indice);
        }
        await filosofo.pegar(this.garfoB(filosofo));
        if (this.delegate != null) {
            this.delegate.pegouGarfoB(indice);
            this.delegate.estaComendo(indice);
        }
        await filosofo.comer();
        if (this.delegate != null) {
            this.delegate.comeu(indice);
        }
        this.rodar(filosofo);
    }

    private garfoA(filosofo: Filosofo) {
        let indice = this.filosofos.indexOf(filosofo);
        if (indice == 0) {
            indice = this.garfos.length;
        }
        return this.garfos[indice - 1];
    }

    private garfoB(filosofo: Filosofo) {
        let indice = this.filosofos.indexOf(filosofo);
        return this.garfos[indice];
    }
}

export interface JantarDelegate {
    dormiu(filosfo: number): void;
    acordou(filosofo: number): void;
    pegouGarfoA(filosofo: number): void;
    pegouGarfoB(filosofo: number): void;
    estaComendo(filosofo: number): void;
    comeu(filosofo: number): void;
}

export class JantarLogger implements JantarDelegate {
    public dormiu(filosofo: number) {
        console.log(`filosofo ${filosofo} dormiu`);
    }

    public acordou(filosofo: number) {
        console.log(`filoso ${filosofo} acordou`);
    }

    public pegouGarfoA(filosofo: number) {
        console.log(`filosofo ${filosofo} pegou garfoA`);
    }

    public pegouGarfoB(filosofo: number) {
        console.log(`filosofo ${filosofo} pegou garfoB`);
    }

    public estaComendo(filosofo: number) {
        console.log(`filosofo ${filosofo} está comendo`);
    }

    public comeu(filosofo: number) {
        console.log(`filosofo ${filosofo} comeu`);
    }
}

export class FilosofoRenderer {
    public garfoA: boolean;
    public garfoB: boolean;
    public isDormindo: boolean;
    public isComendo: boolean;

    public constructor() {
        this.garfoA = false;
        this.garfoB = false;
        this.isDormindo = false;
        this.isComendo = false;
    }
}

export class JantarRenderer implements JantarDelegate {
    private _quantidade: number;
    private _canvas: HTMLCanvasElement;
    private _context: CanvasRenderingContext2D;
    private _filosofos: FilosofoRenderer[];

    public get quantidade(): number {
        return this._quantidade;
    }

    public get canvas(): HTMLCanvasElement {
        return this._canvas;
    }

    public get context(): CanvasRenderingContext2D {
        return this._context;
    }

    public get filosofos(): FilosofoRenderer[] {
        return this._filosofos;
    }

    public constructor(canvas: HTMLCanvasElement, quantidade: number) {
        this._quantidade = quantidade;
        this._canvas = canvas;
        this._context = this.canvas.getContext('2d')!;
        this._filosofos = [];
        for (var i = 0; i < quantidade; i++) {
            this._filosofos.push(new FilosofoRenderer());
        }
    }

    public desenhar() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.desenharMesa(this.context);
        this.desenharPratos(this.context);
        this.desenharGarfos(this.context);
        this.desenharFilosofos(this.context);
    }

    private desenharMesa(context: CanvasRenderingContext2D) {
        let x = this.canvas.width / 2;
        let y = this.canvas.height / 2;
        let raio = Math.min(x, y) / 5 * 3;
        context.beginPath();
        context.arc(x, y, raio, 0, 2 * Math.PI);
        context.stroke();
    }

    private desenharPratos(context: CanvasRenderingContext2D) {
        let x = this.canvas.width / 2;
        let y = this.canvas.height / 2;
        let hora = Math.PI * 2 / this.quantidade;
        let distancia = Math.min(x, y) / 7 * 2;
        for (var i = 0; i < this.quantidade; i++) {
            let angulo = hora * i;
            console.log(i);
            console.log(angulo);
            context.save();
            context.beginPath();
            context.translate(x, y);
            context.rotate(angulo);
            context.translate(distancia, distancia);
            context.rotate(-Math.PI / 4);
            context.rect(-20, -20, 40, 40);
            context.rect(-10, -10, 20, 20);
            context.stroke();
            context.restore();
        }
    }

    private desenharGarfos(context: CanvasRenderingContext2D) {
        let raio = 30;
        let x = this.canvas.width / 2;
        let y = this.canvas.height / 2;
        let hora = Math.PI * 2 / this.quantidade;
        let distancia = Math.min(x, y) / 7 * 2;
        for (var i = 0; i < this.quantidade; i++) {
            let angulo = hora * i - hora / 2;
            console.log(i);
            console.log(angulo);
            context.save();
            context.beginPath();
            context.translate(x, y);
            context.rotate(angulo);
            context.translate(distancia, distancia);
            context.rotate(-Math.PI / 4);
            context.rect(-2.5, 0, 5, 20);
            context.stroke();
            context.restore();
        }
    }

    private desenharFilosofos(context: CanvasRenderingContext2D) {
        let raio = 30;
        let x = this.canvas.width / 2;
        let y = this.canvas.height / 2;
        let hora = Math.PI * 2 / this.quantidade;
        let distancia = Math.min(x, y) / 7 * 4;
        for (var i = 0; i < this.quantidade; i++) {
            let angulo = hora * i;
            let filosofo = this.filosofos[i];
            console.log(i);
            console.log(angulo);
            context.save();
            context.beginPath();
            context.translate(x, y);
            context.rotate(angulo);
            context.translate(distancia, distancia);
            context.arc(0, 0, raio, 0, 2 * Math.PI);
            context.rotate(-Math.PI / 4);
            if (filosofo.isDormindo) {
                context.strokeText('Z Z Z Z Z', -20, 0);
            }
            if (filosofo.isComendo) {
                context.strokeText('H M M M', -20, 0);
            }
            if (filosofo.garfoA) {
                context.rect(+40, -100, 10, 100);
            }
            if (filosofo.garfoB) {
                context.rect(-50, -100, 10, 100);
            }
            context.stroke();
            context.restore();
        }
    }

    public dormiu(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.isDormindo = true;
        this.desenhar();
    }

    public acordou(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.isDormindo = false;
        this.desenhar();
    }

    public pegouGarfoA(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.garfoA = true;
        this.desenhar();
    }

    public pegouGarfoB(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.garfoB = true;
        this.desenhar();
    }

    public estaComendo(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.isComendo = true;
        this.desenhar();
    }

    public comeu(filosofo: number) {
        let _filosofo = this.filosofos[filosofo];
        _filosofo.isComendo = false;
        _filosofo.garfoA = false;
        _filosofo.garfoB = false;
        this.desenhar();
    }
}

let canvas = document.createElement('canvas');
canvas.width = 500;
canvas.height = 500;
document.body.appendChild(canvas);

let quantidade = 5;
let renderer = new JantarRenderer(canvas, quantidade);
let jantar = new Jantar(quantidade);
jantar.delegate = renderer;
renderer.desenhar();
jantar.comecar();

