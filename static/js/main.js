var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
define(["require", "exports"], function (require, exports) {
    "use strict";
    function delay(milliseconds) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                setTimeout(resolve, milliseconds);
            });
        });
    }
    exports.delay = delay;
    (function (Estado) {
        Estado[Estado["dormindo"] = 0] = "dormindo";
        Estado[Estado["esperando"] = 1] = "esperando";
        Estado[Estado["comendo"] = 2] = "comendo";
    })(exports.Estado || (exports.Estado = {}));
    var Estado = exports.Estado;
    class Filosofo {
        constructor() {
            this._estado = Estado.esperando;
            this._garfoA = null;
            this._garfoB = null;
        }
        get estado() {
            return this._estado;
        }
        get garfoA() {
            return this._garfoA;
        }
        get garfoB() {
            return this._garfoB;
        }
        isDormindo() {
            return this._estado == Estado.dormindo;
        }
        isComendo() {
            return this._estado == Estado.comendo;
        }
        isMaosOcupadas() {
            return this.garfoA != null && this.garfoB != null;
        }
        dormir() {
            return __awaiter(this, void 0, void 0, function* () {
                if (this.isDormindo()) {
                    throw new Error();
                }
                this._estado = Estado.dormindo;
                yield delay(this.gerarIntervalo());
                this._estado = Estado.esperando;
            });
        }
        comer() {
            return __awaiter(this, void 0, void 0, function* () {
                if (this.isComendo()) {
                    throw new Error();
                }
                if (!this.isMaosOcupadas()) {
                    throw new Error();
                }
                this._estado = Estado.comendo;
                yield delay(this.gerarIntervalo());
                this._estado = Estado.esperando;
                this.soltarGarfos();
            });
        }
        pegar(garfo) {
            return __awaiter(this, void 0, void 0, function* () {
                if (this.isMaosOcupadas()) {
                    throw new Error();
                }
                if (garfo.isOcupado()) {
                    yield delay(100);
                    yield this.pegar(garfo);
                    return;
                }
                garfo.pegar(this);
                if (this.garfoA == null) {
                    this._garfoA = garfo;
                }
                else {
                    this._garfoB = garfo;
                }
            });
        }
        soltarGarfos() {
            if (this.garfoA != null) {
                this.garfoA.soltar(this);
                this._garfoA = null;
            }
            if (this.garfoB != null) {
                this.garfoB.soltar(this);
                this._garfoB = null;
            }
        }
        gerarIntervalo() {
            return Math.random() * 1000;
        }
    }
    exports.Filosofo = Filosofo;
    class Garfo {
        constructor() {
            this._filosofo = null;
        }
        get filosofo() {
            return this._filosofo;
        }
        isOcupado() {
            return this.filosofo != null;
        }
        pegar(filosofo) {
            if (this.filosofo != null) {
                throw new Error();
            }
            this._filosofo = filosofo;
        }
        soltar(filosofo) {
            if (this.filosofo != filosofo) {
                throw new Error();
            }
            this._filosofo = null;
        }
    }
    exports.Garfo = Garfo;
    class Jantar {
        constructor(quantidade) {
            this._filosofos = [];
            this._garfos = [];
            this._comecou = false;
            this.delegate = null;
            for (var i = 0; i < quantidade; i++) {
                this._filosofos.push(new Filosofo());
                this._garfos.push(new Garfo());
            }
        }
        get filosofos() {
            return this._filosofos;
        }
        get garfos() {
            return this._garfos;
        }
        get comecou() {
            return this._comecou;
        }
        comecar() {
            if (this.comecou) {
                throw new Error();
            }
            this.filosofos.forEach((filosofo) => {
                this.rodar(filosofo);
            });
        }
        rodar(filosofo) {
            return __awaiter(this, void 0, void 0, function* () {
                let indice = this.filosofos.indexOf(filosofo);
                if (this.delegate != null) {
                    this.delegate.dormiu(indice);
                }
                yield filosofo.dormir();
                if (this.delegate != null) {
                    this.delegate.acordou(indice);
                }
                yield filosofo.pegar(this.garfoA(filosofo));
                if (this.delegate != null) {
                    this.delegate.pegouGarfoA(indice);
                }
                yield filosofo.pegar(this.garfoB(filosofo));
                if (this.delegate != null) {
                    this.delegate.pegouGarfoB(indice);
                    this.delegate.estaComendo(indice);
                }
                yield filosofo.comer();
                if (this.delegate != null) {
                    this.delegate.comeu(indice);
                }
                this.rodar(filosofo);
            });
        }
        garfoA(filosofo) {
            let indice = this.filosofos.indexOf(filosofo);
            if (indice == 0) {
                indice = this.garfos.length;
            }
            return this.garfos[indice - 1];
        }
        garfoB(filosofo) {
            let indice = this.filosofos.indexOf(filosofo);
            return this.garfos[indice];
        }
    }
    exports.Jantar = Jantar;
    class JantarLogger {
        dormiu(filosofo) {
            console.log(`filosofo ${filosofo} dormiu`);
        }
        acordou(filosofo) {
            console.log(`filoso ${filosofo} acordou`);
        }
        pegouGarfoA(filosofo) {
            console.log(`filosofo ${filosofo} pegou garfoA`);
        }
        pegouGarfoB(filosofo) {
            console.log(`filosofo ${filosofo} pegou garfoB`);
        }
        estaComendo(filosofo) {
            console.log(`filosofo ${filosofo} está comendo`);
        }
        comeu(filosofo) {
            console.log(`filosofo ${filosofo} comeu`);
        }
    }
    exports.JantarLogger = JantarLogger;
    class FilosofoRenderer {
        constructor() {
            this.garfoA = false;
            this.garfoB = false;
            this.isDormindo = false;
            this.isComendo = false;
        }
    }
    exports.FilosofoRenderer = FilosofoRenderer;
    class JantarRenderer {
        constructor(canvas, quantidade) {
            this._quantidade = quantidade;
            this._canvas = canvas;
            this._context = this.canvas.getContext('2d');
            this._filosofos = [];
            for (var i = 0; i < quantidade; i++) {
                this._filosofos.push(new FilosofoRenderer());
            }
        }
        get quantidade() {
            return this._quantidade;
        }
        get canvas() {
            return this._canvas;
        }
        get context() {
            return this._context;
        }
        get filosofos() {
            return this._filosofos;
        }
        desenhar() {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.desenharMesa(this.context);
            this.desenharPratos(this.context);
            this.desenharGarfos(this.context);
            this.desenharFilosofos(this.context);
        }
        desenharMesa(context) {
            let x = this.canvas.width / 2;
            let y = this.canvas.height / 2;
            let raio = Math.min(x, y) / 5 * 3;
            context.beginPath();
            context.arc(x, y, raio, 0, 2 * Math.PI);
            context.stroke();
        }
        desenharPratos(context) {
            let x = this.canvas.width / 2;
            let y = this.canvas.height / 2;
            let hora = Math.PI * 2 / this.quantidade;
            let distancia = Math.min(x, y) / 7 * 2;
            for (var i = 0; i < this.quantidade; i++) {
                let angulo = hora * i;
                console.log(i);
                console.log(angulo);
                context.save();
                context.beginPath();
                context.translate(x, y);
                context.rotate(angulo);
                context.translate(distancia, distancia);
                context.rotate(-Math.PI / 4);
                context.rect(-20, -20, 40, 40);
                context.rect(-10, -10, 20, 20);
                context.stroke();
                context.restore();
            }
        }
        desenharGarfos(context) {
            let raio = 30;
            let x = this.canvas.width / 2;
            let y = this.canvas.height / 2;
            let hora = Math.PI * 2 / this.quantidade;
            let distancia = Math.min(x, y) / 7 * 2;
            for (var i = 0; i < this.quantidade; i++) {
                let angulo = hora * i - hora / 2;
                console.log(i);
                console.log(angulo);
                context.save();
                context.beginPath();
                context.translate(x, y);
                context.rotate(angulo);
                context.translate(distancia, distancia);
                context.rotate(-Math.PI / 4);
                context.rect(-2.5, 0, 5, 20);
                context.stroke();
                context.restore();
            }
        }
        desenharFilosofos(context) {
            let raio = 30;
            let x = this.canvas.width / 2;
            let y = this.canvas.height / 2;
            let hora = Math.PI * 2 / this.quantidade;
            let distancia = Math.min(x, y) / 7 * 4;
            for (var i = 0; i < this.quantidade; i++) {
                let angulo = hora * i;
                let filosofo = this.filosofos[i];
                console.log(i);
                console.log(angulo);
                context.save();
                context.beginPath();
                context.translate(x, y);
                context.rotate(angulo);
                context.translate(distancia, distancia);
                context.arc(0, 0, raio, 0, 2 * Math.PI);
                context.rotate(-Math.PI / 4);
                if (filosofo.isDormindo) {
                    context.strokeText('Z Z Z Z Z', -20, 0);
                }
                if (filosofo.isComendo) {
                    context.strokeText('H M M M', -20, 0);
                }
                if (filosofo.garfoA) {
                    context.rect(+40, -100, 10, 100);
                }
                if (filosofo.garfoB) {
                    context.rect(-50, -100, 10, 100);
                }
                context.stroke();
                context.restore();
            }
        }
        dormiu(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.isDormindo = true;
            this.desenhar();
        }
        acordou(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.isDormindo = false;
            this.desenhar();
        }
        pegouGarfoA(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.garfoA = true;
            this.desenhar();
        }
        pegouGarfoB(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.garfoB = true;
            this.desenhar();
        }
        estaComendo(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.isComendo = true;
            this.desenhar();
        }
        comeu(filosofo) {
            let _filosofo = this.filosofos[filosofo];
            _filosofo.isComendo = false;
            _filosofo.garfoA = false;
            _filosofo.garfoB = false;
            this.desenhar();
        }
    }
    exports.JantarRenderer = JantarRenderer;
    let canvas = document.createElement('canvas');
    canvas.width = 500;
    canvas.height = 500;
    document.body.appendChild(canvas);
    let quantidade = 5;
    let renderer = new JantarRenderer(canvas, quantidade);
    let jantar = new Jantar(quantidade);
    jantar.delegate = renderer;
    renderer.desenhar();
    jantar.comecar();
});
